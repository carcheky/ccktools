@echo off
pushd "%~dp0"
@echo on
echo "newvhost"
@echo off
wsl --distribution WslServer.18.04 --user user bash /home/user/ccktools/bin/vhost-manager
wsl --distribution WslServer.18.04 --user user /home/user/ccktools/bin/wsl-notify "VHOST OPERATION FINISHED"
pause
:exit
exit
::popd
@echo on
